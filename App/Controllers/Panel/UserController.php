<?php
namespace App\Controllers\Panel;

use App\Services\View\View;

class UserController{

    public function index($request)
    {
        $data = [];
        View::load('panel.user.index', $data, 'panel-admin');
    }

    public function add($request)
    {
        $data = [];
        View::load('panel.user.add', $data, 'panel-admin');
    }

    public function profile($request)
    {
        $data = [];
        View::load('panel.user.profile', $data, 'panel-admin');
    }

    public function addresses($request)
    {
        $data = [];
        View::load('panel.user.addresses', $data, 'panel-admin');
    }


}