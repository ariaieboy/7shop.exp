<?php
namespace App\Models;

class Review extends BaseModel {
	protected $table = 'reviews' ;
	protected $primaryKey = 'id' ;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}