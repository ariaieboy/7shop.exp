<?php
namespace App\Controllers\Panel;

use App\Services\View\View;

class ProductController{

	public function all($request) {
		$data = [];

		View::load('panel.product.all',$data,'panel-admin');
	}
	
	public function add($request) {
		$data = [];
        View::load('panel.product.add',$data,'panel-admin');
	}
	public function save($request) {
		$data = [
			'title' => $request->param('title')
		];
		// data load ..
        View::load('panel.product.save',$data,'panel-admin');
	}
	
}