<?php
namespace App\Models;

class AttributeValue extends BaseModel {
	protected $table = 'attribute_values' ;
	protected $primaryKey = 'id' ;

    public function attribute()
    {
        return $this->belongsTo(Attribute::class, 'attribute_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

}