<?php
namespace App\Models;

class Attribute extends BaseModel {
	protected $table = 'attributes' ;
	protected $primaryKey = 'id' ;

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_attributes', 'attribute_id', 'category_id');
    }

    public function values()
    {
        return $this->hasMany(AttributeValue::class);
    }

}