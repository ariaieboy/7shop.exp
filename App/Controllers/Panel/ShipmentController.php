<?php
namespace App\Controllers\Panel;

use App\Services\View\View;

class ShipmentController{

    public function index($request)
    {
        $data = [];
        View::load('panel.shipment.index', $data, 'panel-admin');
    }


}