<?php
return [
    // Panel Dashboard Routes
    '/panel' => [
        'method' => 'get',
        'target' => 'Panel\DashboardController@index',
    ],
    '/panel/dashboard' => [
        'method' => 'get',
        'target' => 'Panel\DashboardController@index',
    ],

    // Product Routes
    '/panel/products/all' => [
        'method' => 'get',
        'target' => 'Panel\ProductController@all',
    ],
    '/panel/product/add' => [
        'method' => 'get',
        'target' => 'Panel\ProductController@add',
    ],
    '/panel/product/save' => [
        'method' => 'post',
        'target' => 'Panel\ProductController@save',
    ],

    // Media Routes
    '/panel/media' => [
        'method' => 'get',
        'target' => 'Panel\MediaController@index',
    ],
    '/panel/media/add' => [
        'method' => 'get',
        'target' => 'Panel\MediaController@add',
    ],

    // Comment Routes
    '/panel/comments' => [
        'method' => 'get',
        'target' => 'Panel\CommentController@index',
    ],

    // Category and Attribute Routes
    '/panel/categories' => [
        'method' => 'get',
        'target' => 'Panel\CategoryController@index',
    ],
    '/panel/categories/attributes' => [
        'method' => 'get',
        'target' => 'Panel\CategoryController@attributes',
    ],
    '/panel/attributes' => [
        'method' => 'get',
        'target' => 'Panel\AttributeController@index',
    ],

    // Order Routes
    '/panel/orders' => [
        'method' => 'get',
        'target' => 'Panel\OrderController@index',
    ],

    // Shipment Routes
    '/panel/shipments' => [
        'method' => 'get',
        'target' => 'Panel\ShipmentController@index',
    ],

    // Payment Routes
    '/panel/payments' => [
        'method' => 'get',
        'target' => 'Panel\PaymentController@index',
    ],

    // User Routes
    '/panel/users' => [
        'method' => 'get',
        'target' => 'Panel\UserController@index',
    ],
    '/panel/users/add' => [
        'method' => 'get',
        'target' => 'Panel\UserController@add',
    ],
    '/panel/user/profile' => [
        'method' => 'get',
        'target' => 'Panel\UserController@profile',
    ], '/panel/user/addresses' => [
        'method' => 'get',
        'target' => 'Panel\UserController@addresses',
    ],

    // Option Routes
    '/panel/options' => [
        'method' => 'get',
        'target' => 'Panel\OptionController@index',
    ],
    '/panel/options/add' => [
        'method' => 'get',
        'target' => 'Panel\OptionController@add',
    ],
    '/panel/options/create' => [
        'method' => 'post',
        'target' => 'Panel\OptionController@create',
    ],
];

