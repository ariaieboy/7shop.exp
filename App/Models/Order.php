<?php
namespace App\Models;

class Order extends BaseModel {
	protected $table = 'orders' ;
	protected $primaryKey = 'id' ;

    public function shipment(){
        return $this->hasOne(Shipment::class);
    }
    public function payments(){
        return $this->hasMany(Payment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_products', 'order_id', 'product_id');
    }

}